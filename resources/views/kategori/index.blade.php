@extends('layout.master')

@section('judul')
Halaman List Kategori
@endsection


@section('content')

<a href="/kategori/create" class="btn btn-success mb-3">Tambah Data</a>

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">List Berita</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
       @forelse ($kategori as $key => $item)
            <tr>
                <td>{{$key + 1}}</td>
                <td>{{$item->nama}}</td>
                <td>{{$item->content}}</td>
                <td>
                    <ul>
                        @foreach ($item->berita as $value)
                            <li>{{$value->judul}}</li>
                        @endforeach
                    </ul>
                </td>

                <td>
                    <form action="/kategori/{{$item->id}}" method="POST">
                        <a href="/kategori/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm ml-2">Edit</a>
                        @method('delete')
                        @csrf
                        <input type="submit" class="btn btn-danger btn-sm ml-2" value="Delete">
                    </form>
                </td>
            </tr>
       @empty
       <tr>
           <td>Data Masih Kosong</td>
       </tr>
       @endforelse
    </tbody>
</table>
@endsection