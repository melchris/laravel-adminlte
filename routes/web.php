<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/index', function () {
//     return view('halaman.index');
// });

Route::get('/', 'DashboardController@index');

Route::get('/form', 'FormController@bio');

Route::post('/welcome', 'FormController@Submit');

Route::get('/master', function(){
    return view('layout.master');
});

Route::get('/data-table', function(){
    return view('tables.data-table');
});

Route::get('/table', function(){
    return view('tables.table');
});

Route::group(['middleware' => ['auth']], function () {
    // CRUD Kategori
    Route::get('/kategori/create', 'KategoriController@create');
    Route::post('/kategori', 'KategoriController@store');
    Route::get('/kategori', 'KategoriController@index');
    Route::get('/kategori/{kategori_id}', 'KategoriController@show');
    Route::get('/kategori/{kategori_id}/edit', 'KategoriController@edit');
    Route::put('/kategori/{kategori_id}', 'KategoriController@update');
    Route::delete('/kategori/{kategori_id}', 'KategoriController@destroy');

    //CRUD PROFILE
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('komentar', 'KomentarController')->only([
        'index', 'store'
    ]);
});

// CRUD BERITA
Route::resource('berita', 'BeritaController');

Auth::routes();

